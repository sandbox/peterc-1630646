<?php
/**
 * @file
 *   Provides admin settings form
 */

/**
 * General settings form
 */
function mythtvapi_client_settings_form($form) {

  drupal_set_title(t('Mythtv Api Client general settings'));

  $form['backend_api_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Api backend url'),
    '#default_value' => variable_get('backend_api_url'),
    '#description' => t('Example: http://BackendServerIP:6544'),
    '#required' => TRUE,
  );

  return system_settings_form($form);
}