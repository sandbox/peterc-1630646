<?php
/**
 * @file
 *   Provides http://www.mythtv.org/wiki/DVR_Service
 */

class MythtvApiClientDvr extends MythtvApiClient {

  protected $service_name = 'Dvr';

  /**
   * Query information on all upcoming programs matching recording rules.
   *
   * @param integer $start_index
   *   The numerical starting index in the list of upcoming programs.
   * @param integer $count
   *   The number of items to return in the result.
   * @param boolean $show_all
   *   Show all items which match recording rules, regardless of whether or not they will record (conflicts, previously recorded, etc.).
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function getUpcomingList($start_index = NULL, $count = NULL, $show_all = NULL) {
    $params = array();

    if ($start_index) {
      $params['StartIndex'] = (int) $start_index;
    }
    if ($count) {
      $params['Count'] = (int) $count;
    }
    if ($show_all) {
      $params['ShowAll'] = 'true';
    }
    else {
      $params['ShowAll'] = 'false';
    }

    try {
      $obj = $this->call('GetUpcomingList', $params);
      if (isset($obj->ProgramList)) {
        return $obj->ProgramList;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Query information on recorded programs which are set to expire.
   *
   * @param integer $start_index
   *   The numerical starting index in the list of expiring programs.
   * @param integer $count
   *   The number of items to return in the result.
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function getExpiringList($start_index = NULL, $count = NULL) {
    $params = array();

    if ($start_index) {
      $params['StartIndex'] = (int) $start_index;
    }
    if ($count) {
      $params['Count'] = (int) $count;
    }

    try {
      $obj = $this->call('GetExpiringList', $params);
      if (isset($obj->ProgramList)) {
        return $obj->ProgramList;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Query information on all recorded programs.
   *
   * @param string $order
   *   Return results in ascending (asc) or descending (desc) chronological order.
   * @param integer $start_index
   *   The numerical starting index in the list of expiring programs.
   * @param integer $count
   *   The number of items to return in the result.
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function getRecordedList($order = 'asc', $start_index = NULL, $count = NULL) {
    $params = array();

    if (drupal_strtolower($order) != 'asc') {
      $params['Descending'] = TRUE;
    }
    if ($start_index) {
      $params['StartIndex'] = (int) $start_index;
    }
    if ($count) {
      $params['Count'] = (int) $count;
    }

    try {
      $obj = $this->call('GetRecordedList', $params);
      if (isset($obj->ProgramList)) {
        return $obj->ProgramList;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Query information on a single item from recordings.
   *
   * @param integer $chan_id
   *   The database channel id for the guide item.
   * @param string $start_time
   *   The recording start time for the item.
   *   This should be in MySQL ISO format, eg: 2011-08-29 18:59:00.
   *   You can replace the space with %20 or T.
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function getRecorded($chan_id, $start_time) {
    $params = array();

    $params['ChanId'] = (int) $chan_id;
    $params['StartTime'] = $start_time;

    try {
      $obj = $this->call('GetRecorded', $params);
      if (isset($obj->Program)) {
        return $obj->Program;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Query information on upcoming items which will not record due to conflicts.
   *
   * @param integer $start_index
   *   The numerical starting index in the list of expiring programs.
   * @param integer $count
   *   The number of items to return in the result.
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function getConflictList($start_index = NULL, $count = NULL) {
    $params = array();

    if ($start_index) {
      $params['StartIndex'] = (int) $start_index;
    }
    if ($count) {
      $params['Count'] = (int) $count;
    }

    try {
      $obj = $this->call('GetConflictList', $params);
      if (isset($obj->Program)) {
        return $obj->Program;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Remove a Recording from the database and disk.
   *
   * @todo Get specs from the api.
   *   Specs not available at http://www.mythtv.org/wiki/DVR_Service#RemoveRecorded
   */
  public function removeRecorded() {

  }

  /**
   * Query information on configured capture devices, and their current activity.
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function getEncoderList() {
    try {
      $obj = $this->call('GetEncoderList');
      if (isset($obj->EncoderList)) {
        return $obj->EncoderList;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Remove a Recording rule.
   *
   * @param integer $record_id
   *   The database id of the recording rule to be removed.
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function removeRecordSchedule($record_id) {
    $params = array();

    $params['RecordId'] = (int) $record_id;

    try {
      $obj = $this->call('RemoveRecordSchedule', $params);
      if (isset($obj->bool) && $obj->bool == 'true') {
        return TRUE;
      }
      return FALSE;
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Query all configured recording rules, and return them in a list.
   *
   * @param integer $start_index
   *   The numerical starting index in the list of expiring programs.
   * @param integer $count
   *   The number of items to return in the result.
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function getRecordScheduleList($start_index = NULL, $count = NULL) {
    $params = array();

    if ($start_index) {
      $params['StartIndex'] = (int) $start_index;
    }
    if ($count) {
      $params['Count'] = (int) $count;
    }

    try {
      $obj = $this->call('GetRecordScheduleList', $params);
      if (isset($obj->RecRuleList)) {
        return $obj->RecRuleList;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Return a single recording rule, by record id.
   *
   * @param integer $record_id
   *   The database id of the recording rule to be removed.
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function getRecordSchedule($record_id) {
    $params = array();

    $params['RecordId'] = (int) $record_id;

    try {
      $obj = $this->call('GetRecordSchedule', $params);
      if (isset($obj->RecRule)) {
        return $obj->RecRule;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Enable a recording schedule, by record id.
   *
   * @param integer $record_id
   *   The database id of the recording rule to be removed.
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function enableRecordSchedule($record_id) {
    $params = array();

    $params['RecordId'] = (int) $record_id;

    try {
      $obj = $this->call('EnableRecordSchedule', $params);
      if (isset($obj->bool) && $obj->bool == 'true') {
        return TRUE;
      }
      return FALSE;
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Disable a recording schedule, by record id.
   *
   * @param integer $record_id
   *   The database id of the recording rule to be removed.
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function disableRecordSchedule($record_id) {
    $params = array();

    $params['RecordId'] = (int) $record_id;

    try {
      $obj = $this->call('DisableRecordSchedule', $params);
      if (isset($obj->bool) && $obj->bool == 'true') {
        return TRUE;
      }
      return FALSE;
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

}

