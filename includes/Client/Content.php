<?php
/**
 * @file
 *   Provides http://www.mythtv.org/wiki/Content_Service
 */

class MythtvApiClientContent extends MythtvApiClient {

  protected $service_name = 'Content';

  /**
   * Display a preview thumbnail for a given recording by chanid and starttime.
   *
   * @param string $start_time
   *   $Program->Recording->StartTs
   *
   * @return string
   *   The return for this command is a binary file which will be interpreted by your browser
   *   and downloaded or displayed as appropriate.
   * @throws MythtvServiceException
   */
  public function getPreviewImage($chan_id, $start_time, $width = NULL, $height = NULL, $secs_in = NULL) {
    $params = array();

    if ($width) {
      $params['Width'] = (int) $width;
    }
    if ($height) {
      $params['Height'] = (int) $height;
    }
    if ($secs_in) {
      $params['SecsIn'] = (int) $secs_in;
    }

    try {
      // Call but stream the result directly to the browser
      $this->call('GetPreviewImage', $params, TRUE);
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

}

