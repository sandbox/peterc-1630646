<?php
/**
 * @file
 *   Provides http://www.mythtv.org/wiki/Video_Service
 */

class MythtvApiClientVideo extends MythtvApiClient {

  protected $service_name = 'Video';

  /**
   * Get a list of videos and their metadata.
   *
   * @param string $order
   *   Return results in ascending (asc) or descending (desc) order.
   * @param Integer $start_index
   *   The first index number to return.
   * @param Integer $count
   *   The number of records to return.
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function getVideoList($order = 'asc', $start_index = NULL, $count = NULL) {
    $params = array();
    if (drupal_strtolower($order) != 'asc') {
      $params['Descending'] = TRUE;
    }
    if ($count) {
      $params['Count'] = (int) $count;
    }
    if ($start_index) {
      $params['StartIndex'] = (int) $start_index;
    }
    try {
      $obj = $this->call('GetVideoList', $params);
      if (isset($obj->VideoMetadataInfoList)) {
        return $obj->VideoMetadataInfoList;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Get a single video's metadata by database id.
   *
   * @param Integer $id The database id of the requested metadata record.
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function getVideo($id) {
    try {
      $obj = $this->call('GetVideo', array('Id' => (int) $id));
      if (isset($obj->VideoMetadataInfo)) {
        return $obj->VideoMetadataInfo;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Get a single video's metadata by filename.
   *
   * @param String $filename The filename of the requested metadata record.
   *
   * @return object False on failure
   * @throws MythtvServiceException
   */
  public function getVideoByFileName($filename) {
    try {
      $obj = $this->call('GetVideoByFileName', array('FileName' => $filename));
      if (isset($obj->VideoMetadataInfo)) {
        return $obj->VideoMetadataInfo;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Add a new filename to the video database.
   *
   * @param String $filename
   *   The filename (including relative path from the root of the video storage group) to be added to the database.
   * @param String $hostname
   *   The backend hostname where the file can be found.
   *
   * @return bool
   * @throws MythtvServiceException
   */
  public function addVideo($filename, $hostname) {
    try {
      $obj = $this->call('GetVideoByFileName', array('FileName' => $filename, 'HostName' => $hostname));
      if (isset($obj->bool) && $obj->bool == 'true') {
        return TRUE;
      }
      return FALSE;
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Remove a single video from the database by database id.
   *
   * @param Integer $id
   *   The database id of the metadata record to be removed.
   *
   * @return bool
   * @throws MythtvServiceException
   */
  public function removeVideoFromDb($id) {
    try {
      $obj = $this->call('RemoveVideoFromDB', array('Id' => (int) $id));
      if (isset($obj->bool) && $obj->bool == 'true') {
        return TRUE;
      }
      return FALSE;
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Perform an online metadata lookup for a given title, subtitle, inetref, season, or episode number.
   *
   * @param String $title
   *   The title to be looked up.
   * @param String $subtitle
   *   The subtitle to be looked up.
   * @param String $inetref
   *   The internet reference number to be looked up.
   * @param Integer $season
   *   The season to be looked up.
   * @param String $episode
   *   The episode number to be looked up.
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function lookupVideo($title, $subtitle = NULL, $inetref = NULL, $season = 0, $episode = NULL) {
    $params = array('Title' => $title);
    if ($descending) {
      $params['Descending'] = TRUE;
    }
    if (!empty($subtitle)) {
      $params['Subtitle'] = $subtitle;
    }
    if (!empty($inetref)) {
      $params['Inetref'] = $inetref;
    }
    if (!empty($season)) {
      $params['Season'] = (int) $season;
    }
    if (!empty($episode)) {
      $params['Episode'] = $episode;
    }
    try {
      $obj = $this->call('LookupVideo', $params);
      if (isset($obj->VideoLookupList)) {
        return $obj->VideoLookupList;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Query technical data for a Blu-ray disc or folder.
   *
   * @param String $path
   *   The path to a Blu-ray drive or folder structure.
   *
   * @return object
   * @throws MythtvServiceException
   */
  public function getBluray($path) {
    try {
      $obj = $this->call('GetBluray', array('Path' => $path));
      if (isset($obj->BlurayInfo)) {
        return $obj->BlurayInfo;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }
}

