<?php
/**
 * @file
 *   Provides $service_host:$service_post/Myth/wsdl
 *   (currently no page at http://www.mythtv.org/wiki/Myth_Service)
 */

class MythtvApiClientMyth extends MythtvApiClient {

  protected $service_name = 'Myth';

  /**
   * Adds a Storage group directory
   *
   * @param string $group_name
   * @param string $dir_name
   * @param string $host_name
   * @return boolean
   * @throws MythtvServiceException
   */
  public function addStorageGroupDir($group_name, $dir_name, $host_name) {
    $params = array();
    $params['GroupName'] = $group_name;
    $params['DirName'] = $dir_name;
    $params['HostName'] = $host_name;
    try {
      $obj = $this->call('AddStorageGroupDir', $params);
      if (isset($obj->bool) && $obj->bool == 'true') {
        return TRUE;
      }
      return FALSE;
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Backs up the database
   *
   * @returns boolean
   * @throws MythtvServiceException
   */
  public function backupDatabase() {
    try {
      $obj = $this->call('BackupDatabase');
      if (isset($obj->bool) && $obj->bool == 'true') {
        return TRUE;
      }
      return FALSE;
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Gets the host name
   *
   * @return string
   * @throws MythtvServiceException
   */
  public function getHostName() {
    try {
      $obj = $this->call('GetHostName');
      if (isset($obj->String)) {
        return $obj->String;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }

  /**
   * Get a list of known hosts
   *
   * @return array
   * @throws MythtvServiceException
   */
  public function getHosts() {
    try {
      $obj = $this->call('GetHosts');
      if (isset($obj->StringList)) {
        return $obj->StringList;
      }
      throw new MythtvApiClientException(__FUNCTION__ . ': unexpected response');
    } catch (MythtvApiClientException $e) {
      throw $e;
    }
  }
}
