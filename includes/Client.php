<?php
/**
 * @file
 *   Provides the base class for clients.
 */

class MythtvApiClient {

  protected $api_url;
  protected $service_name;

  public static function getClient($type, $api_url = NULL) {

    if (empty($api_url)) {
      $api_url = variable_get('backend_api_url', NULL);
    }

    if (empty($api_url)) {
      throw new MythtvApiClientException('Api url not set');
    }

    $registered_clients = self::getRegisteredClients();
    if (in_array($type, $registered_clients)) {
      if (include_once 'Client/' . $type . '.php') {
        $classname = 'MythtvApiClient' . $type;
        return new $classname($api_url);
      }
    }

    throw new MythtvApiClientException('Service not defined');
  }

  public static function getRegisteredClients() {
    return array(
      'Content',
      'Dvr',
      'Myth',
      'Video',
    );
  }

  public function __construct($api_url) {
    $this->api_url = $api_url;
  }

  /**
   * Calls the service with the appropriate parameters
   *
   * @todo Check bug reports for json errors returned as xml.
   */
  public function call($function, $params = array(), $stream = FALSE) {
    if (empty($this->service_name)) {
      throw new MythtvApiClientException('No service name given to service call');
    }

    $url = $this->api_url . '/' . $this->service_name . '/';
    $url .= $function;
    if (count($params) > 0) {
      $url .=  '?';
      foreach ($params as $key => $value) {
        $url .= '&' . $key . '=' . $value;
      }
    }

    $context_opts = array(
      'http' => array(
        'method'  => 'POST',
        'content' => '',
        'timeout' => 10.5,
        'ignore_errors' => TRUE,
      )
    );
    $context = stream_context_create($context_opts);

    $options = array(
      'method' => 'POST',
      'timeout' => 30.5,
      'context' => $context,
      'headers' => array(
        'Content-type' => 'application/x-www-form-urlencoded',
        'Accept' => 'application/json',
        'User-Agent' => 'MythtvApiClient',
      )
    );

    if ($stream) {
      // Pass the response directly to the browser.
      // Use for files, especially large ones such as videos.
      self::stream_http($url, $options);
    }
    else {
      // Collect the response in memory & process it.
      $result = drupal_http_request($url, $options);

      if ($result->code == '200') {
        $json = json_decode($result->data);
        // Errors are returned as xml.
        if (json_last_error() == JSON_ERROR_NONE) {
          return $json;
        }
        // see if we have an xml error message
        if ($result->data[0] == '<') {
          $xml = new SimpleXMLElement($result->data);
          if (isset($xml->errorDescription)) {
            throw new MythtvApiClientException((string) $xml->errorDescription, (string) $xml->errorCode);
          }
        }
      }

      if (isset($result->error)) {
        $msg = $result->error;
      }
      elseif (isset($result->status_message)) {
        $msg = $result->status_message;
      }
      else {
        $msg = 'Unkown error';
      }
      throw new MythtvApiClientException($msg, $result->code);
    }
  }

  public static function stream_http($url, $options = array()) {
    drupal_session_commit();
    if (ob_get_level()) {
      ob_end_clean();
    }

    // Take a large chunk of code from drupal_http_request() without timeout...

    // Parse the URL and make sure we can handle the schema.
    $uri = @parse_url($url);

    //timer_start(__FUNCTION__);

    // Merge the default options.
    $options += array(
      'headers' => array(),
      'method' => 'POST',
      'data' => NULL,
      'max_redirects' => 3,
      'timeout' => 30.0,
      'context' => NULL,
    );
    // stream_socket_client() requires timeout to be a float.
    $options['timeout'] = (float) $options['timeout'];

    $port = isset($uri['port']) ? $uri['port'] : 80;
      $socket = 'tcp://' . $uri['host'] . ':' . $port;
      // RFC 2616: "non-standard ports MUST, default ports MAY be included".
      // We don't add the standard port to prevent from breaking rewrite rules
      // checking the host that do not take into account the port number.
      $options['headers']['Host'] = $uri['host'] . ($port != 80 ? ':' . $port : '');


    if (empty($options['context'])) {
      $fp = @stream_socket_client($socket, $errno, $errstr, $options['timeout']);
    }
    else {
      // Create a stream with context. Allows verification of a SSL certificate.
      $fp = @stream_socket_client($socket, $errno, $errstr, $options['timeout'], STREAM_CLIENT_CONNECT, $options['context']);
    }

    if ($fp) {

      // Construct the path to act on.
      $path = isset($uri['path']) ? $uri['path'] : '/';
      if (isset($uri['query'])) {
        $path .= '?' . $uri['query'];
      }

      $request = $options['method'] . ' ' . $path . " HTTP/1.0\r\n";
      foreach ($options['headers'] as $name => $value) {
        $request .= $name . ': ' . trim($value) . "\r\n";
      }
      $request .= "\r\n";

      fwrite($fp, $request);

      // Fetch response. Due to PHP bugs like http://bugs.php.net/bug.php?id=43782
      // and http://bugs.php.net/bug.php?id=46049 we can't rely on feof(), but
      // instead must invoke stream_get_meta_data() each iteration.
      $info = stream_get_meta_data($fp);
      $alive = !$info['eof'] && !$info['timed_out'];

      // End of big copy & paste from drupal_http_request()

      while ($alive) {

        $chunk = fread($fp, 1024);

        // reset the timeout for php execution
        set_time_limit(30);

        if (!empty($header_end)) {
          // send on to the calling application
          print $chunk;
        }
        else {
          if (!isset($buffer)) {
            $buffer = '';
          }
          $buffer .= $chunk;
          $header_end = strpos($buffer, "\r\n\r\n");
          if ($header_end) {
            // Get the headers
            $header = substr($buffer, 0, $header_end);

            // Remove ALL headers queued for sending
            $headers_list = headers_list();
            foreach ($headers_list as $headers_item) {
              $parts = explode(':', $headers_item);
              header_remove($parts[0]);
            }

            // Set the headers as received
            $headers = self::http_parse_headers($header);
            foreach ($headers as $name => $value) {
              header($name . ': ' . $value);
            }

            // Send the rest of the chunk that is not part of the headers
            print substr($buffer, $header_end + 4);
            unset($buffer);
          }
        }

        $info = stream_get_meta_data($fp);
        $alive = !$info['eof'] && !$info['timed_out'] && $chunk;
      }
      fclose($fp);
      exit;
    }
    // TODO: handle $fp error

  }

  public static function http_parse_headers($header) {
    $ret = array();
    $fields = explode("\r\n", preg_replace('/\x0D\x0A[6\x09\x20]+/', ' ', $header));
    foreach( $fields as $i => $field ) {
      if ($i == 0) {
        //$ret['status'] = $field;
      }
      if( preg_match('/([^:]+): (.+)/m', $field, $match) ) {
        $match[1] = preg_replace('/(?<=^|[\x09\x20\x2D])./e', 'strtoupper("\0")', strtolower(trim($match[1])));
        if( isset($retVal[$match[1]]) ) {
          $ret[$match[1]] = array($ret[$match[1]], $match[2]);
        } else {
          $ret[$match[1]] = trim($match[2]);
        }
      }
    }
    return $ret;
  }


}

class MythtvApiClientException extends Exception { }
